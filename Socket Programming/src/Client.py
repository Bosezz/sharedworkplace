from tkinter import * #@UnusedWildImport
from tkinter.ttk import * #@UnusedWildImport
import time
import socket
import threading
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding

class Client:
    def __init__(self, ip, port):
        self.ip = ip
        self.port = int(port)

        threading.Thread(target=self.createWindow).start()
        time.sleep(0.1)
        threading.Thread(target=self.socketThread).start()
        
    def createWindow(self):
        #creates the Window
        self.window = Tk()
        self.window.title("Server on" + self.ip + ":" + str(self.port))
        
        self.chatTextBox = Text(self.window, height = 10, width = 60)
        self.chatTextBox.grid(column = 1, row = 0, rowspan = 2, columnspan = 2)
        
        self.connectionsTextBox = Text(self.window, height = 12, width = 20)
        self.connectionsTextBox.grid(column = 0, row = 0, rowspan = 3)
        
        self.inputTextBox = Entry(self.window, width = 65)
        self.inputTextBox.grid(column = 1, row = 2)
        
        self.sendButton = Button(self.window, text="Send", command = self.sendData)
        self.sendButton.grid(column = 2, row = 2)

        self.window.mainloop()
        
    def socketThread(self):
        #creating privateKey and publicKey
        self.createCryptographyKeys()
        
        #creating the socket and connect it to the Server
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect_ex((self.ip, self.port))
        self.writeText("Connection successfully with Server", self.ip +":"+ str(self.port))
        
        #send public key to Server
        self.socket.sendall(self.publicKeyBytes)
        
        #get public key from Server
        serverPublicKeyBytes = b''
        while True:
            serverPublicKeyBytes += self.socket.recv(512);
            if len(serverPublicKeyBytes) == 451:
                break
            
        self.serverPublicKey = serialization.load_pem_public_key(
            serverPublicKeyBytes, 
            backend=default_backend()
            )
        
        #process incoming date
        self.processData()
        
    def sendData(self):
        self.writeText("You", self.inputTextBox.get())
        data = self.serverPublicKey.encrypt(
            self.inputTextBox.get().encode(),
            padding.OAEP(
                mgf=padding.MGF1(algorithm=hashes.SHA512()),
                algorithm=hashes.SHA512(),
                label = None
                )
            )
        self.socket.sendall(data)
        
    def processData(self):
        try:
            while True:
                receivedData = self.socket.recv(1024)
                if receivedData:
                    decryptedMessage = self.privateKey.decrypt(
                        receivedData,
                        padding.OAEP(
                            mgf=padding.MGF1(algorithm=hashes.SHA512()),
                            algorithm=hashes.SHA512(),
                            label = None
                            )
                        )
                    self.writeText("Server", decryptedMessage.decode())
                else:
                    self.writeText("Warning", "Lost connection to the Server")
                    return
        except(ConnectionAbortedError):
            self.writeText("ConnectionAborted", "Lost connection to the Server")     
        except OSError as err:
            self.writeText("OSError", err)
        finally:
            self.socket.close()
  
    def writeText(self, author, text):   
        self.chatTextBox.insert(END, str(author) +": "+ str(text) + "\n")
        
    def createCryptographyKeys(self):
        #creating the private Key
        self.privateKey = rsa.generate_private_key(
            public_exponent=65537,
            key_size=2048, 
            backend=default_backend()
        )
        
        #generate the public Key from the private key
        self.publicKey = self.privateKey.public_key()
        
        #convert the public Key into Bytes to send it to the Server later
        self.publicKeyBytes = self.publicKey.public_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PublicFormat.SubjectPublicKeyInfo,
        )
        
    def close(self):
        self.window.destroy()
        self.socket.close()