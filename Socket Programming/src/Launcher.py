import sys
from PySide2.QtWidgets import QApplication, QMainWindow, QPushButton, QLineEdit
from PySide2.QtUiTools import QUiLoader
from PySide2.QtCore import QFile
from Client import Client
from Server import Server
import threading

class Launcher(QMainWindow):

    def __init__(self):
        self.createWindow()
        
    def createWindow(self):
        
        app = QApplication(sys.argv)

        ui_file = QFile("GUI/Launcher.ui")
        ui_file.open(QFile.ReadOnly)
        
        loader = QUiLoader()
        self.window = loader.load(ui_file)
        ui_file.close()
        self.window.show()
        
        self.window.findChild(QPushButton, "createServer").clicked.connect(self.createServer)
        self.window.findChild(QPushButton, "createClient").clicked.connect(self.createClient)
        
        sys.exit(app.exec_())
        
        
    def createClient(self):
        threading.Thread(target=self.runClient).start()
        
    def runClient(self):
        ip = self.window.findChild(QLineEdit, "ipTextBox").text()
        port = self.window.findChild(QLineEdit, "portTextBox").text()
        print("Launcher is starting Client: " + ip +":"+ port)
        Client(ip, port)
    
    def createServer(self):
        threading.Thread(target=self.runServer).start()
        
    def runServer(self):
        ip = self.window.findChild(QLineEdit, "ipTextBox").text()
        port = self.window.findChild(QLineEdit, "portTextBox").text()
        print("Launcher is starting Server: " + ip +":"+ port)
        Server(ip, port)
