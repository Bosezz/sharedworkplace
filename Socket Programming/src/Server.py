from tkinter import * #@UnusedWildImport
from tkinter.ttk import * #@UnusedWildImport
import socket
import threading
import selectors
import time
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding

class Server:
    def __init__(self, ip, port):
        self.ip = ip
        self.port = int(port)
        
        threading.Thread(target=self.createWindow).start()
        threading.Thread(target=self.socketThread).start()
    
    def createWindow(self):
        
        self.window = Tk()
        self.window.title("Server on" + self.ip + ":" + str(self.port))
        
        self.chatTextBox = Text(self.window, height = 10, width = 60)
        self.chatTextBox.grid(column = 1, row = 0, rowspan = 2, columnspan = 2)
        
        self.connectionsTextBox = Text(self.window, height = 12, width = 20)
        self.connectionsTextBox.grid(column = 0, row = 0, rowspan = 3)
        
        self.inputTextBox = Entry(self.window, width = 65)
        self.inputTextBox.grid(column = 1, row = 2)
        
        self.sendButton = Button(self.window, text="Send", command = self.sendData)
        self.sendButton.grid(column = 2, row = 2)

        self.window.mainloop()
        
    def socketThread(self):
        self.createCryptographyKeys()
        
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        
        self.selector = selectors.DefaultSelector()
        
        self.socket.bind((self.ip, self.port))
        self.socket.listen()
        self.socket.setblocking(False)
        
        self.selector.register(self.socket, selectors.EVENT_READ, data="Server")
        
        self.processData()
        
    def processData(self):
        while True:
            try:
                events = self.selector.select()
            except OSError as err:
                self.writeText("OSError", err)
                self.socket.close()
                break
            for key, mask in events:
                #key contains the socket object and the data.
                #mask contains the bitwise operators (read and write) that need to be done.
                if key.data == "Server":
                    self.registerNewSocket()
                else:
                    self.handleConnection(key, mask)
         
    def registerNewSocket(self):
        try:
            newSocket, address = self.socket.accept()
        except(WindowsError):
            self.close()
            return
        
        newSocket.setblocking(False)
        
        time.sleep(0.5)
        
        publicKeyBytes = b''
        try:
            while True:
                publicKeyBytes += newSocket.recv(512);
                if len(publicKeyBytes) == 451:
                    break
        except Exception as err:
            self.writeText("Connection failed with ", address[0] +":"+ str(address[1]) + " because " + str(err))
            return

        publicKey = serialization.load_pem_public_key(
            publicKeyBytes, 
            backend=default_backend()
            )
        
        newSocket.sendall(self.publicKeyBytes)

        events = selectors.EVENT_READ | selectors.EVENT_WRITE
        data = { 
            "address" : address,
            "publicKey" : publicKey,
            "input" : b'',
            "output" : b''     
                }

        self.selector.register(newSocket, events, data)
        
        self.writeText("Connection established with", address[0] +":"+ str(address[1]))
    
    def handleConnection(self, key, mask):
        handledSocket = key.fileobj
        data = key.data
        
        if mask & selectors.EVENT_READ:
            try:
                receivedData = handledSocket.recv(1024)
                if receivedData:
                    decodedMessage = self.privateKey.decrypt(
                        receivedData,
                        padding.OAEP(
                            mgf=padding.MGF1(algorithm=hashes.SHA512()),
                            algorithm=hashes.SHA512(),
                            label = None
                            )
                        )
                    self.writeText(data["address"][0], decodedMessage.decode())
                else:
                    self.writeText("Lost connection to", data["address"])
                    self.selector.unregister(handledSocket)
                    handledSocket.close()
            except(OSError):
                self.writeText("Lost connection to", data["address"])
                self.selector.unregister(handledSocket)
                handledSocket.close()

        if mask & selectors.EVENT_WRITE:
            if data["output"]:
                self.writeText("You", (data["output"].decode()))
                decodedMessage = data["publicKey"].encrypt(
                    data["output"],
                    padding.OAEP(
                        mgf=padding.MGF1(algorithm=hashes.SHA512()),
                        algorithm=hashes.SHA512(),
                        label = None
                        )
                    )
                handledSocket.sendall(decodedMessage)
                data["output"] = b''
        
    def writeText(self, author, text):   
        self.chatTextBox.insert(END, str(author) +": "+ str(text) + "\n")
    
    def sendData(self):
        for ID in self.selector.get_map():
            selectorKey = self.selector._key_from_fd(ID)
            data = selectorKey.data
            if data == "Server":
                continue
            data["output"] = self.inputTextBox.get().encode()
            
    def createCryptographyKeys(self):
        #creating the private Key
        self.privateKey = rsa.generate_private_key(
            public_exponent=65537,
            key_size=2048, 
            backend=default_backend()
        )
    
        #generate the public Key from the private key
        self.publicKey = self.privateKey.public_key()
        
        #convert the public Key into Bytes to send it to the Server later
        self.publicKeyBytes = self.publicKey.public_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PublicFormat.SubjectPublicKeyInfo,
        )
        
    def close(self):
        self.window.destroy()
        self.socket.close()