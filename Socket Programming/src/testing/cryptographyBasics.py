from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding

#generate a private key, this should not be shared to anyone.
privateKey = rsa.generate_private_key(
    public_exponent=65537,
    key_size=2048, 
    backend=default_backend()
    )
print("privateKey: ", privateKey)

#extract the public key from the private key.
publicKey = privateKey.public_key()
print("publicKey: ", publicKey)

#convert the private key into bytes.
privateKeyBytes = privateKey.private_bytes(
    encoding=serialization.Encoding.PEM,
    format=serialization.PrivateFormat.TraditionalOpenSSL,
    encryption_algorithm=serialization.NoEncryption())
print("privateKeyBytes: ", privateKeyBytes)
print("privateKeyBytes size: ", len(privateKeyBytes))

#convert the public key into bytes.
publicKeyBytes = publicKey.public_bytes(
    encoding=serialization.Encoding.PEM,
    format=serialization.PublicFormat.SubjectPublicKeyInfo,
    )
print("publicKeyBytes: ", publicKeyBytes)
print("publicKeyBytes size: ", len(publicKeyBytes))

#convert the privateKeyBytes back to a key
privateKeyDecrypted = serialization.load_pem_private_key(
    privateKeyBytes,
    backend=default_backend(),
    password=None
    )
print("privateKeyDecrypted: ", privateKeyDecrypted)

#convert the publicKeyBytes back to a key
publicKeyDecrypted = serialization.load_pem_public_key(
    publicKeyBytes, 
    backend=default_backend())
print("publicKeyDecrypted: ", publicKeyDecrypted)

print("-----------------------------------------------------------------------------------------")

plainText = "Hello World"
print("plainText: ", plainText)

plainTextBytes = plainText.encode()
print("plainTextBytes: ", plainTextBytes)

#encrypt plainText with the public Key
cipherText = publicKey.encrypt(
    plainTextBytes,
    padding.OAEP(
        mgf=padding.MGF1(algorithm=hashes.SHA512()),
        algorithm=hashes.SHA512(),
        label = None
        )
    )
print("cipherText: ", cipherText)

#encrypt plainText with the public Key (testing correct conversion)
cipherText2 = publicKeyDecrypted.encrypt(
    plainTextBytes,
    padding.OAEP(
        mgf=padding.MGF1(algorithm=hashes.SHA512()),
        algorithm=hashes.SHA512(),
        label = None
        )
    )
print("cipherText2: ", cipherText2)

#decrypt plainText with the private Key
cipherTextEncrypter = privateKey.decrypt(
    cipherText,
    padding.OAEP(
        mgf=padding.MGF1(algorithm=hashes.SHA512()),
        algorithm=hashes.SHA512(),
        label = None
        )
    )
print("cipherTextEncrypter: ", cipherTextEncrypter)

#decrypt plainText with the private Key (testing correct conversion)
cipherTextEncrypter2 = privateKeyDecrypted.decrypt(
    cipherText2,
    padding.OAEP(
        mgf=padding.MGF1(algorithm=hashes.SHA512()),
        algorithm=hashes.SHA512(),
        label = None
        )
    )
print("cipherTextEncrypter2: ", cipherTextEncrypter2)




